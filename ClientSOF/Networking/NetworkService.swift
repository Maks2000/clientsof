//
//  NetworkService.swift
//  ClientSOF
//
//  Created by Максим Сидорин on 14.10.2021.
//

import Foundation

class NetworkService {
    
    func request(urlString: String, completion: @escaping (Result <SearchResponse, Error>) -> Void) {
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error)  in
    
            DispatchQueue.main.async {
                if let error = error {
                    print("Some Error")
                    completion(.failure(error))
                    return
                }
                guard let data = data else { return }
                do{
                    let myQuestions = try JSONDecoder().decode(SearchResponse.self, from: data)
                    completion(.success(myQuestions))
                }
                catch let jsonError{
                    print("Failed to decode JSON", jsonError)
                    completion(.failure(jsonError))
                }
            }
        }.resume()
    }
}
