//
//  AnswerCollectionViewCell.swift
//  ClientSOF
//
//  Created by Максим Сидорин on 19.10.2021.
//

import UIKit

class AnswerCollectionViewCell: UICollectionViewCell {
        
    
    //-------------Answers items-------
    
    public var answerView : UIView = {
       var answerView = UIView()
        answerView.translatesAutoresizingMaskIntoConstraints = false
     return answerView
    }()
    
    private let answerLabel : UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
       return label
    }()
    
    private var answerBody : UITextView = {
       var answerBody = UITextView()
        answerBody.translatesAutoresizingMaskIntoConstraints = false
     return answerBody
    }()
    
    private let answerUserName : UILabel = {
       let answerUserName = UILabel()
        answerUserName.translatesAutoresizingMaskIntoConstraints = false
     return answerUserName
    }()
    
    private let answerProfileImage : UIImageView = {
       let answerProfileImage = UIImageView()
        answerProfileImage.translatesAutoresizingMaskIntoConstraints = false
     return answerProfileImage
    }()
    
    private var answerCreationDate : UILabel = {
       var answerCreationDate = UILabel()
        answerCreationDate.translatesAutoresizingMaskIntoConstraints = false
     return answerCreationDate
    }()
    
    private var answerScore : UILabel = {
       var answerScore = UILabel()
        answerScore.translatesAutoresizingMaskIntoConstraints = false
     return answerScore
    }()
    
    
    public func configureAnswerView(){
        
        contentView.addSubview(answerView)
        
        answerView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        answerView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        answerView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        answerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    
    }
    
    public func configureAnswerLabel(numberAnswer: Int){
        
        answerView.addSubview(answerLabel)
        
        answerLabel.text = "Answer: \(numberAnswer) "
    
        answerLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        answerLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        
        answerLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        
        answerLabel.font = UIFont(name: answerLabel.font.familyName, size: 20)
        answerLabel.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    public func configureAnswerBody(anwerItem : myAnswers) {
        
        answerBody.text = anwerItem.body
        
        answerView.addSubview(answerBody)
        
        answerBody.backgroundColor = UIColor.lightGray
        answerBody.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        answerBody.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -40).isActive = true
        answerBody.heightAnchor.constraint(equalToConstant: 250).isActive = true
        answerBody.topAnchor.constraint(equalTo: answerLabel.bottomAnchor,constant: 20).isActive = true
        answerBody.font = UIFont(name: answerLabel.font.familyName, size: 16)
    }
    
    public func configureUserAnswerName(anwerItem : myAnswers) {
    
        answerView.addSubview(answerUserName)
        
        answerUserName.text = anwerItem.owner.display_name
        
        let contentSize = self.answerUserName.sizeThatFits(self.answerUserName.bounds.size)
        
        answerUserName.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
        answerUserName.widthAnchor.constraint(equalToConstant: contentSize.width).isActive = true
        answerUserName.heightAnchor.constraint(equalToConstant: contentSize.height).isActive = true
        answerUserName.topAnchor.constraint(equalTo: answerBody.bottomAnchor).isActive = true
        answerUserName.font = UIFont(name: answerUserName.font.familyName, size: 16)
        answerUserName.textColor = UIColor.orange
        answerUserName.numberOfLines = 0
    }
    
    public func configureAnswerUserImage(anwerItem : myAnswers){
        
        answerProfileImage.image = UIImage(named: "item?.answers.owner.profile_image")
        if let url = URL(string: anwerItem.owner.profile_image ?? "") {
               if let data = try? Data(contentsOf: url) {
                   answerProfileImage.image = UIImage(data: data)
               }
        }
        
        answerView.addSubview(answerProfileImage)
        
        answerProfileImage.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
        answerProfileImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        answerProfileImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        answerProfileImage.topAnchor.constraint(equalTo: answerUserName.bottomAnchor).isActive = true
    }
    
    public func configureAnswerCreationDate(anwerItem : myAnswers) {
    
        answerView.addSubview(answerCreationDate)
        
        let date = Date(timeIntervalSince1970: anwerItem.creation_date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        answerCreationDate.text = strDate
        
        answerCreationDate.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
        answerCreationDate.widthAnchor.constraint(equalToConstant: 120).isActive = true
        answerCreationDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        answerCreationDate.bottomAnchor.constraint(equalTo: answerProfileImage.bottomAnchor).isActive = true
        answerCreationDate.font = UIFont(name: answerCreationDate.font.familyName, size: 16)
        answerCreationDate.textColor = UIColor.gray
    }
    
    public func configureAnswerScore(anwerItem : myAnswers) {
        
        answerView.addSubview(answerScore)
        
        answerScore.text = "Score of answer: " + String(anwerItem.score)
        
        answerScore.leftAnchor.constraint(equalTo: answerCreationDate.rightAnchor, constant: 20).isActive = true
        answerScore.widthAnchor.constraint(equalToConstant: 170).isActive = true
        answerScore.heightAnchor.constraint(equalToConstant: 20).isActive = true
        answerScore.bottomAnchor.constraint(equalTo: answerProfileImage.bottomAnchor).isActive = true
        answerScore.font = UIFont(name: answerCreationDate.font.familyName, size: 16)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
