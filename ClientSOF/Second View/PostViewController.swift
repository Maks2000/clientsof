//
//  PostViewController.swift
//  ClientSOF
//
//  Created by Максим Сидорин on 16.10.2021.
//

import UIKit
import SafariServices

class PostViewController: UIViewController {
    
    
    var postElements : oneQuestion?
    let contentView = UIView()

    var scrollView = UIScrollView()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let scrollItem = UILabel(frame: CGRect(x: 0, y: 0 , width: 0, height: 0))
        
        scrollView = UIScrollView(frame: CGRect(x: 10, y: 0, width: self.view.frame.size.width - 10, height: self.view.frame.height))
        self.view.addSubview(scrollView)
        scrollView.addSubview(scrollItem)
    
        scrollItem.topAnchor.constraint(equalTo: view.topAnchor, constant: 170).isActive = true
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
    
        self.addQuestionSubview()
        self.questionConfig()
  
        scrollView.addSubview(collectionView ?? UICollectionView())
        
        collectionViewConfig()

        scrollView.contentSize = CGSize(width: view.frame.size.width, height: view.frame.size.height + 500)
    }
    
    private func addQuestionSubview() {
        scrollView.addSubview(questionTitle)
        self.scrollView.addSubview(questionBody)
        self.scrollView.addSubview(questionUserName)
        self.scrollView.addSubview(questionProfileImage)
        self.scrollView.addSubview(questionCreationDate)
    }
    
    
    private func questionConfig() {
        configureTitleQuestion(item: postElements)
        configureBodyQuestion(item: postElements)
        configureUserQuestionName(item: postElements)
        configureQusetionUserImage(item: postElements)
        configureQusetionCreationDate(item: postElements)
    }
    
    let collectionView: UICollectionView? = {
            let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
            cv.translatesAutoresizingMaskIntoConstraints = false
            return cv;
        }();
    
    //-------------Question items-------
    
    private let questionTitle : UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
       return label
    }()
    
    private let questionBody : UITextView = {
       let bodyQuestion = UITextView()
        bodyQuestion.translatesAutoresizingMaskIntoConstraints = false
     return bodyQuestion
    }()
    
    private let questionUserName : UILabel = {
       let userQuestionName = UILabel()
        userQuestionName.translatesAutoresizingMaskIntoConstraints = false
     return userQuestionName
    }()
    
    private let questionProfileImage : UIImageView = {
       let questionProfileImage = UIImageView()
        questionProfileImage.translatesAutoresizingMaskIntoConstraints = false
     return questionProfileImage
    }()
    
    private var questionCreationDate : UILabel = {
       var questionCreationDate = UILabel()
        questionCreationDate.translatesAutoresizingMaskIntoConstraints = false
     return questionCreationDate
    }()
   
    
    private func collectionViewConfig() {
        
        collectionView?.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        collectionView?.topAnchor.constraint(equalTo:  questionCreationDate.bottomAnchor, constant: 30).isActive = true;
        collectionView?.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true; // height
        collectionView?.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true; // width
        collectionView?.register(AnswerCollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
        collectionView?.backgroundColor = UIColor.white
    }
  
    //-------------Question configs-------

    public func configureTitleQuestion(item : oneQuestion?) {
        
        questionTitle.text = item?.title
    
        questionTitle.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20).isActive = true
        questionTitle.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -40).isActive = true
        questionTitle.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 150).isActive = true
        questionTitle.font = UIFont(name: questionTitle.font.familyName, size: 20)
        questionTitle.font = UIFont.boldSystemFont(ofSize: 20)
        questionTitle.numberOfLines = 0
    }

    public func configureBodyQuestion(item : oneQuestion?) {
        
        questionBody.text = item?.body
        
        let contentSize = self.questionBody.sizeThatFits(self.questionBody.bounds.size)
        questionBody.backgroundColor = UIColor.lightGray
        questionBody.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20).isActive = true
        questionBody.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: -40).isActive = true
        questionBody.heightAnchor.constraint(equalToConstant: contentSize.height + 30).isActive = true
        questionBody.topAnchor.constraint(equalTo: questionTitle.bottomAnchor).isActive = true
        questionBody.font = UIFont(name: questionTitle.font.familyName, size: 16)
    }
    
    public func configureUserQuestionName(item : oneQuestion?) {
        
        questionUserName.text = item?.owner.display_name
    
        let contentSize = self.questionUserName.sizeThatFits(self.questionUserName.bounds.size)
        
        questionUserName.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -20).isActive = true
        questionUserName.widthAnchor.constraint(equalToConstant: contentSize.width).isActive = true
        questionUserName.heightAnchor.constraint(equalToConstant: contentSize.height).isActive = true
        questionUserName.topAnchor.constraint(equalTo: questionBody.bottomAnchor).isActive = true
        questionUserName.font = UIFont(name: questionUserName.font.familyName, size: 16)
        questionUserName.textColor = UIColor.orange
        questionUserName.numberOfLines = 0
    }
    
    public func configureQusetionUserImage(item : oneQuestion?){
        
        questionProfileImage.image = UIImage(named: "item?.owner.profile_image")
        if let url = URL(string: item?.owner.profile_image ?? " ") {
               if let data = try? Data(contentsOf: url) {
                   questionProfileImage.image = UIImage(data: data)
               }
        }
    
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        questionProfileImage.isUserInteractionEnabled = true
        questionProfileImage.addGestureRecognizer(tapGestureRecognizer)
        
        questionProfileImage.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -20).isActive = true
        questionProfileImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        questionProfileImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        questionProfileImage.topAnchor.constraint(equalTo: questionUserName.bottomAnchor).isActive = true
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {

        let url = URL(string: postElements?.owner.link ?? "")!
        
        let safariVC = SFSafariViewController(url: url)
        present(safariVC, animated: true, completion: nil)
    }
    
    public func configureQusetionCreationDate(item : oneQuestion?) {
        
        questionCreationDate.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20).isActive = true
        questionCreationDate.widthAnchor.constraint(equalToConstant: 120).isActive = true
        questionCreationDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        questionCreationDate.topAnchor.constraint(equalTo: questionBody.bottomAnchor, constant: 50).isActive = true
        questionCreationDate.font = UIFont(name: questionCreationDate.font.familyName, size: 16)
        questionCreationDate.textColor = UIColor.gray
        
        let date = Date(timeIntervalSince1970: item?.creation_date ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        
        questionCreationDate.text = strDate
    }
}

extension PostViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postElements?.answer_count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width: view.bounds.width, height: view.bounds.height / 2.2)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as!AnswerCollectionViewCell
        
        let item = postElements?.answers?[indexPath.row]
        
        cell.configureAnswerView()
        cell.configureAnswerLabel(numberAnswer: indexPath.row + 1)
        cell.configureAnswerBody(anwerItem: item!)
        cell.configureUserAnswerName(anwerItem: item!)
        cell.configureAnswerUserImage(anwerItem: item!)
        cell.configureAnswerCreationDate(anwerItem: item!)
        cell.configureAnswerScore(anwerItem: item!)
        return cell
    }
}

extension PostViewController: UICollectionViewDelegate {
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       print("User tapped on item \(indexPath.row)")
    }
}
