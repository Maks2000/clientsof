//
//  ViewController.swift
//  ClientSOF
//
//  Created by Максим Сидорин on 13.10.2021.
//

import UIKit

class ViewController: UIViewController {
      
    @IBOutlet var table: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var searchResponse: SearchResponse? = nil
    let networkService = NetworkService()
    private var timer : Timer?
    
    override func viewDidLoad() {
        
        for constraint in self.view.constraints {
                  constraint.isActive = false
             }
        
        
        super.viewDidLoad()
        
        setupTableView()
        setupSearchController()

    }
    
    private func setupSearchController() {
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        navigationController?.navigationBar.prefersLargeTitles = true
        searchController.obscuresBackgroundDuringPresentation = false
    }
    
    private func setupTableView() {
        table.delegate = self
        table.dataSource = self
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResponse?.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "myQuestion", for: indexPath) as!oneQuestionTableViewCell
             
        let item = searchResponse?.items?[indexPath.row]
        
        cell.configureLabel(item: item)
        cell.configureImage(item: item)
        cell.configureUserName(item: item)
        cell.configureQuestionDate(item: item)
        cell.configureAnswerCount(item: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let postViewController = self.storyboard?.instantiateViewController(withIdentifier: "PVC") as! PostViewController
  
        show(postViewController,sender: nil)
        postViewController.postElements = searchResponse?.items?[indexPath.row]
    }
}


extension ViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
           let urlString =   "https://api.stackexchange.com/2.3/search?order=desc&sort=activity&intitle=\(searchText)&site=stackoverflow&filter=!6VvPDzQ)wlg1u"
    
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
            self.networkService.request(urlString: urlString) {[weak self] (result) in
                switch result {
                    
                case .success(let searchResponse):
                    
                    self?.searchResponse = searchResponse
                    self?.table.reloadData()

                case .failure(let error):
                    print("error", error)
                }
            }
        })
    }
}
