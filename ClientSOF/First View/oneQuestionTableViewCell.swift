//
//  oneQuestionTableViewCell.swift
//  ClientSOF
//
//  Created by Максим Сидорин on 14.10.2021.
//

import UIKit

class oneQuestionTableViewCell: UITableViewCell {
    
    
    private let questionTitle : UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
       return label
    }()
    
    private let questionUserName : UILabel = {
       let userQuestionName = UILabel()
        userQuestionName.translatesAutoresizingMaskIntoConstraints = false
     return userQuestionName
    }()
    
    private let questionProfileImage : UIImageView = {
       let questionProfileImage = UIImageView()
        questionProfileImage.translatesAutoresizingMaskIntoConstraints = false
     return questionProfileImage
    }()
    
    private var questionCreationDate : UILabel = {
       var questionCreationDate = UILabel()
        questionCreationDate.translatesAutoresizingMaskIntoConstraints = false
     return questionCreationDate
    }()
    
    private var questionAnswerCount : UILabel = {
       var questionAnswerCount = UILabel()
        questionAnswerCount.translatesAutoresizingMaskIntoConstraints = false
     return questionAnswerCount
    }()
    
    public func configureAnswerCount(item : oneQuestion?){
        contentView.addSubview(questionAnswerCount)
        
        let lstr = "Count of answers: " + NumberFormatter.localizedString(from: NSNumber(value: item?.answer_count ?? 0), number: .decimal)
        questionAnswerCount.text = lstr
    }
    
    public func configureLabel(item : oneQuestion?){
        contentView.addSubview(questionTitle)
        
        questionTitle.text = item?.title
    }
    
    public func configureImage(item : oneQuestion?){
    
        contentView.addSubview(questionProfileImage)
        questionProfileImage.image = UIImage(named: "item?.owner.profile_image")
        if let url = URL(string: item?.owner.profile_image ?? " ") {
               if let data = try? Data(contentsOf: url) {
                           //cell.userImageView2.sizeToFit()
                   questionProfileImage.image = UIImage(data: data)
               }
        }
    }
    
    public func configureUserName(item : oneQuestion?){
        contentView.addSubview(questionUserName)
        
        questionUserName.text = item?.owner.display_name
    }
    
    public func configureQuestionDate(item : oneQuestion?){
        contentView.addSubview(questionCreationDate)
    
        let date = Date(timeIntervalSince1970: item?.creation_date ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        
        questionCreationDate.text = strDate
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        //-------Title---------
        questionTitle.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        questionTitle.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -40).isActive = true
        questionTitle.bottomAnchor.constraint(equalTo: questionUserName.topAnchor).isActive = true
        questionTitle.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        questionTitle.font = UIFont(name: questionTitle.font.familyName, size: 20)
        questionTitle.font = UIFont.boldSystemFont(ofSize: 20)
        questionTitle.numberOfLines = 0
    
        
        //--------Image--------
        questionProfileImage.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
        questionProfileImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        questionProfileImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        questionProfileImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -10).isActive = true
        
        //--------UserName-----
        
        let contentSize = self.questionUserName.sizeThatFits(self.questionUserName.bounds.size)
        
        questionUserName.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
        questionUserName.widthAnchor.constraint(equalToConstant: contentSize.width).isActive = true
        questionUserName.heightAnchor.constraint(equalToConstant: contentSize.height).isActive = true
        questionUserName.bottomAnchor.constraint(equalTo: questionProfileImage.topAnchor).isActive = true
        questionUserName.font = UIFont(name: questionUserName.font.familyName, size: 16)
        questionUserName.textColor = UIColor.orange
        questionUserName.numberOfLines = 0
        
        //--------Date---------
        questionCreationDate.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        questionCreationDate.widthAnchor.constraint(equalToConstant: 120).isActive = true
        questionCreationDate.heightAnchor.constraint(equalToConstant: 20).isActive = true
        questionCreationDate.bottomAnchor.constraint(equalTo: questionProfileImage.bottomAnchor).isActive = true
        questionCreationDate.font = UIFont(name: questionCreationDate.font.familyName, size: 16)
        questionCreationDate.textColor = UIColor.gray
        
        //-------AnswerCounts---
        
        questionAnswerCount.leftAnchor.constraint(equalTo: questionCreationDate.rightAnchor, constant: 10).isActive = true
        questionAnswerCount.widthAnchor.constraint(equalToConstant: 200).isActive = true
        questionAnswerCount.heightAnchor.constraint(equalToConstant: 15).isActive = true
        questionAnswerCount.bottomAnchor.constraint(equalTo: questionProfileImage.bottomAnchor).isActive = true
        questionAnswerCount.font = UIFont(name: questionAnswerCount.font.familyName, size: 18)
    
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
