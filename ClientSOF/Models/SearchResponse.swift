//
//  SearchResponse.swift
//  ClientSOF
//
//  Created by Максим Сидорин on 13.10.2021.
//

import Foundation

struct SearchResponse : Decodable {
   var items: [oneQuestion]?
}

struct oneQuestion : Decodable {
    var answers: [myAnswers]?
    var title: String
    var creation_date: Double
    var answer_count: Int
    var body: String
    var owner: ownerQuestion
}

struct myAnswers: Decodable {
    var body: String?
    var creation_date: Double
    var score: Int
    var owner: ownerAnswer
}

struct ownerQuestion: Decodable {
    var profile_image: String?
    var display_name: String
    var link: String
}

struct ownerAnswer: Decodable {
    var profile_image: String?
    var display_name: String
}
